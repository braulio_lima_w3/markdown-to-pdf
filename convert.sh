# converstion from md to pdf goes on here

echo 'Creating pdf...'

[ -z `command -v pandoc` ] && echo 'Pandoc does not exist. please install it.' && exit 1
[ -z `command -v pdfetex` ] && echo 'Pdfetex does not exist. please install it.' && exit 1

if [ -z "$TEMP_NAME" ]
then
    TEMP_NAME="temporary.md"
fi

if [ -f temporary.md ]
then
    # converts from md to html, reason: md to pdf directly leaves a few problems behind
    pandoc -so final.html "$TEMP_NAME"
    pandoc -so final.pdf final.html

    # cleans up
    echo 'Cleaning up...'
    rm final.html temporary.md;
else
    echo 'Temporary file was not generated, therefore, nothing will be generated'
fi