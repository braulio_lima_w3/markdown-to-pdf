# Converting to markdown

## Dependencies

    $ sudo apt install pandoc
    $ sudo apt-get install texlive-latex-base
    $ sudo apt-get install texlive-fonts-recommended texlive-latex-recommended

## Instructions

1. Place all of your markdown files in the `markdown` folder;
1. Execute the command:

(in bash)

    ./markdown-to-pdf

or

    bash markdown-to-pdf

## Configuration

To change working folder, simply execute:

    MD_SOURCE=/location/to/your/markdown/folder ./markdown-to-pdf

## Notes

The output file name is `final.pdf`

As it stands, this is a fixed value

